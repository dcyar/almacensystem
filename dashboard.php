<!DOCTYPE html>
<html lang="es">
<head>
	<meta charset="UTF-8" />
	<meta name="viewport" content="width=device-width, initial-scale=1.0" />
	<title>Dashboard</title>
	<link href='https://fonts.googleapis.com/css?family=Audiowide' rel='stylesheet' type='text/css' />
	<!-- Compiled and minified CSS -->
	<link rel="stylesheet" href="./css/materialize.min.css" />
	<link rel="stylesheet" href="css/main.css" />
	<link rel="stylesheet" href="css/dashboard.css" />
	<link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet" />
	
</head>
<body>
	<div class="row">
		<div class="col s3 teal lighten-2 padding-bot-top" style="height: 900px;">
			<div class="col s12 border-bot">
				<div class="col s3 no-padding">
					<img class="circle" src="img/avatar.jpg" alt="usuario" style="width: 100px;" />
				</div>
				<div class="col s9 margin-top-small right-align">
					<h3 class="small-font white-text">Username</h3>
					<a href="./" class="blue-text text-lighten-4 .vertical-align-mid">Editar perfil<i class="material-icons icon-setting ">settings</i></a>
				</div>
			</div>
			<div class="col s12 menu border-bot padding-bot-top">
				<a href="dashboard.php" class="padding-left-small white-text padding-bot-top selected-menu" style="display: block;">INICIO</a>
				<a href="insumos.php" class="padding-left-small white-text padding-bot-top" style="display: block;">INSUMOS</a>
				<a href="bebidas.php" class="padding-left-small white-text padding-bot-top" style="display: block;">BEBIDAS</a>
				<a href="bebidas-alcoholicas.php" class="padding-left-small white-text padding-bot-top" style="display: block;">BEBIDAS ALCOHOLICAS</a>
				<a href="extras.php" class="padding-left-small white-text padding-bot-top" style="display: block;">EXTRAS</a>
			</div>
			<div class="col s12 menu padding-bot-top border-bot">
				<a href="#" class="padding-left-small white-text padding-bot-top" style="display: block;">REPORTES</a>
			</div>
			<div class="col s12 menu margin-top-small">
				<div class="col s2 right-align no-padding">
					<img class="logo-j2v" src="img/j2v.png" alt="J2V" />
				</div>
				<div class="col s8 left-align" >
					<p class="white-text"style="font-size: 12px;">Copyright 2016 - Team J2V</p>
				</div>
			</div>
		</div>
		<div class="col padding-left-right padding-bot-top">
			<div class="col s8">
				<div class="card-panel col s12 padding-bot-top">
					<h5 class="center-align">Ventas</h5>
					<table class="striped">
						<thead>
							<tr>
								<th data-field="id">Name</th>
								<th data-field="name">Item Name</th>
								<th data-field="price">Item Price</th>
							</tr>
						</thead>
						<tbody>
							<tr>
								<td>Alvin</td>
								<td>Eclair</td>
								<td>$0.87</td>
							</tr>
							<tr>
								<td>Alan</td>
								<td>Jellybean</td>
								<td>$3.76</td>
							</tr>
							<tr>
								<td>Jonathan</td>
								<td>Lollipop</td>
								<td>$7.00</td>
							</tr>
							<tr>
								<td>Alvin</td>
								<td>Eclair</td>
								<td>$0.87</td>
							</tr>
							<tr>
								<td>Alan</td>
								<td>Jellybean</td>
								<td>$3.76</td>
							</tr>
						</tbody>
					</table>
				</div>
				<div class="card-panel col s12 padding-bot-top">
					<h5 class="center-align">Compras</h5>
					<table class="striped">
						<thead>
							<tr>
								<th data-field="id">Name</th>
								<th data-field="name">Item Name</th>
								<th data-field="price">Item Price</th>
							</tr>
						</thead>
						<tbody>
							<tr>
								<td>Alvin</td>
								<td>Eclair</td>
								<td>$0.87</td>
							</tr>
							<tr>
								<td>Alan</td>
								<td>Jellybean</td>
								<td>$3.76</td>
							</tr>
							<tr>
								<td>Jonathan</td>
								<td>Lollipop</td>
								<td>$7.00</td>
							</tr>
							<tr>
								<td>Alvin</td>
								<td>Eclair</td>
								<td>$0.87</td>
							</tr>
							<tr>
								<td>Alan</td>
								<td>Jellybean</td>
								<td>$3.76</td>
							</tr>
						</tbody>
					</table>
				</div>
			</div>
			
			<div class="card-panel col s4 padding-bot-top">
				<h5 class="center-align">Productos en stock</h5>
				<table class="striped">
						<thead>
							<tr>
								<th data-field="id">Name</th>
								<th data-field="price">Item Price</th>
							</tr>
						</thead>
						<tbody>
							<tr>
								<td>Alvin</td>
								<td>$0.87</td>
							</tr>
							<tr>
								<td>Alan</td>
								<td>$3.76</td>
							</tr>
							<tr>
								<td>Jonathan</td>
								<td>$7.00</td>
							</tr>
							<tr>
								<td>Angel</td>
								<td>$1.00</td>
							</tr>
							<tr>
								<td>Carlos</td>
								<td>$2.30</td>
							</tr>
							<tr>
								<td>Evans</td>
								<td>$11.00</td>
							</tr>
							<tr>
								<td>Choco</td>
								<td>$0.50</td>
							</tr>
							<tr>
								<td>Sideral</td>
								<td>$6.10</td>
							</tr>
							<tr>
								<td>Snow</td>
								<td>$9.99</td>
							</tr>
							<tr>
								<td>Homero</td>
								<td>$20.10</td>
							</tr>
						</tbody>
					</table>
			</div>

			<div class="card-panel col s12 padding-bot-top">
				<h5 class="center-align">Estadisticas</h5>

				aqui ira las estadisticas de los 10 productos mas vendidos
			</div>	
		</div>
    </div>
    <!-- Compiled and minified Jquery -->
	<script src="./js/jquery-2.2.3.min.js"></script>
	<!-- Compiled and minified JavaScript -->
	<script src="./js/materialize.min.js"></script>
</body>
</html>