<!DOCTYPE html>
<html lang="es">
<head>
	<meta charset="UTF-8" />
	<meta name="viewport" content="width=device-width, initial-scale=1.0" />
	<title>Dashboard</title>
	<link href='https://fonts.googleapis.com/css?family=Audiowide' rel='stylesheet' type='text/css' />
	<!-- Compiled and minified CSS -->
	<link rel="stylesheet" href="../css/materialize.min.css" />
	<link rel="stylesheet" href="../css/main.css" />
	<link rel="stylesheet" href="../css/dashboard.css" />
	<link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet" />
	
</head>
<body>
	<div class="row">
		<div class="col s3 teal lighten-2 padding-bot-top" style="height: 900px;">
			<div class="col s12 border-bot">
				<div class="col s3 no-padding">
					<img class="circle" src="../img/avatar.jpg" alt="usuario" style="width: 100px;" />
				</div>
				<div class="col s9 margin-top-small right-align">
					<h3 class="small-font white-text">Username</h3>
					<a href="../" class="blue-text text-lighten-4 .vertical-align-mid">Editar perfil<i class="material-icons icon-setting ">settings</i></a>
				</div>
			</div>
			<div class="col s12 menu border-bot padding-bot-top">
				<a href="../dashboard.php" class="padding-left-small white-text padding-bot-top selected-menu" style="display: block;">INICIO</a>
				<a href="../insumos.php" class="padding-left-small white-text padding-bot-top" style="display: block;">INSUMOS</a>
				<a href="../bebidas.php" class="padding-left-small white-text padding-bot-top" style="display: block;">BEBIDAS</a>
				<a href="../bebidas-alcoholicas.php" class="padding-left-small white-text padding-bot-top" style="display: block;">BEBIDAS ALCOHOLICAS</a>
				<a href="../extras.php" class="padding-left-small white-text padding-bot-top" style="display: block;">EXTRAS</a>
			</div>
			<div class="col s12 menu padding-bot-top border-bot">
				<a href="#" class="padding-left-small white-text padding-bot-top" style="display: block;">REPORTES</a>
			</div>
			<div class="col s12 menu margin-top-small">
				<div class="col s2 right-align no-padding">
					<img class="logo-j2v" src="../img/j2v.png" alt="J2V" />
				</div>
				<div class="col s8 left-align" >
					<p class="white-text"style="font-size: 12px;">Copyright 2016 - Team J2V</p>
				</div>
			</div>
		</div>
		<div class="col s9 padding-left-right padding-bot-top">
			<div class="col s12 padding-bot-top">
				<h3 class="col s6 left-align">Nuevo Insumo</h3>
			</div>
			<div class="col s12">
				<form class="col s6">
			      <div class="row">
			        <div class="input-field col s12">
			        	<select>
							<option value="" disabled selected>Seleccione una opcion</option>
							<option value="1">Insumo</option>
							<option value="2">Bebidas</option>
							<option value="3">Bebidas Alcoholicas</option>
							<option value="4">Extras</option>
						</select>
						<label>Tipo de producto</label>
			        </div>
			        <div class="input-field col s12">
			          <input id="first_name" type="text" class="validate">
			          <label for="first_name">Nombre</label>
			        </div>
			        <div class="input-field col s12">
			          <input id="first_name" type="number" class="validate">
			          <label for="first_name">Stock</label>
			        </div>
			        <div class="input-field col s12">
			          <input name="date" type="date" class="datepicker">
			          <label for="date">Fecha</label>
			        </div>
			        <div class="input-field col s6">
			          <button class="btn waves-effect waves-light" type="submit" name="action">Enviar</button>
			        </div>
			      </div>
			    </form>
			</div>
		</div>
    </div>
    <!-- Compiled and minified Jquery -->
	<script src="../js/jquery-2.2.3.min.js"></script>
	<!-- Compiled and minified JavaScript -->
	<script src="../js/materialize.min.js"></script>
	<script>
		$(document).ready(function() {
		    $('select').material_select();
		  });
		$('.datepicker').pickadate({
		    selectMonths: true, // Creates a dropdown to control month
		    selectYears: 2 // Creates a dropdown of 15 years to control year
		  });
	</script>
</body>
</html>