<!DOCTYPE html>
<html lang="es">
<head>
	<meta charset="UTF-8" />
	<meta name="viewport" content="width=device-width, initial-scale=1.0" />
	<title>AlmacenSystem - login</title>

	<link href='https://fonts.googleapis.com/css?family=Audiowide' rel='stylesheet' type='text/css' />
	<!-- Compiled and minified CSS -->
	<link rel="stylesheet" href="./css/materialize.min.css" />
	<link rel="stylesheet" href="css/main.css" />
	<link rel="stylesheet" href="css/login.css" />
	
</head>
<body>
	<div id="capa"></div>
	<div id="title-page" class="container center margin-top-regular margin-bottom-small">
		<h1 class="white-text extra-big-font">AS</h1>
		<span class="font-bold small-font white-text">Almacen System</span>
	</div>
	<div class="container center login-container">
		<div class="card-panel card-container" style="padding-bottom: .5em;">
			<h3 class="padding-bot-top regular-font">Inicia Sesión</h3>

			<div class="row">
				<form class="col s12 no-margin-bottom" action="login.php">
					<div class="row no-margin-bottom">
						<div class="input-field col s12">
							<input id="email" type="email" class="validate" required >
							<label for="email" data-error="Email no valido" class="left-align">Email</label>
						</div>
					</div>
					<div class="row">
						<div class="input-field col s12">
							<input id="password" type="password" class="validate" required >
							<label for="password" class="left-align">Password</label>
						</div>
					</div>
					<div class="row">
						<div class="input-field col s12">
							<button class="btn waves-effect waves-light col s12" type="submit" >Entrar</button>
						</div>
						
					</div>
				</form>
				<div class="col s12">
					<a href="#">Create new user</a>
				</div>
			</div>
		</div>
	</div>

	<div class="container center login-container white-text">
		<p class="no-margin">Copyright 2016 - Team J2V</p>
		<p class="no-margin"><a href="dashboard.php">www.j2v.com</a></p>
	</div>

	<!-- Compiled and minified Jquery -->
	<script src="./js/jquery-2.2.3.min.js"></script>
	<!-- Compiled and minified JavaScript -->
	<script src="./js/materialize.min.js"></script>
</body>
</html>